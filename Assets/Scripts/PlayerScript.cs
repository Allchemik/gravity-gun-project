﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    public bool exploded;
    public float range;
    public float reload;
    public int activeWeapon;

    public Text WeaponText;


    public GameObject[] weapons;
    public float speed;


    Vector3 direction;

    private LineRenderer laserLine;
    float nextFire;
    bool fired;
    // Use this for initialization
	void Start () {
        laserLine = GetComponent<LineRenderer>();
        nextFire = Time.time;
        WeaponText.text = "Weapon: " + weapons[activeWeapon].name;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 v3 =Input.mousePosition;
        v3.z = 10.0f;
        v3 = Camera.main.ScreenToWorldPoint(v3);


        direction = v3 - transform.position;
        //Input.mousePosition;
        laserLine.SetPosition(0, transform.position);

        if(Input.GetButtonDown("Jump"))
        {
            activeWeapon++;
            activeWeapon %= (weapons.Length);
            WeaponText.text = "Weapon: " + weapons[activeWeapon].name;
        }


        /*if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            activeWeapon++;
            activeWeapon %= (weapons.Length );
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            activeWeapon--;
            activeWeapon %= (weapons.Length );
        }*/



        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, range))
        {
            laserLine.SetPosition(1, hit.point);
        }
        else
        {
            laserLine.SetPosition(1, direction*range);  
        }

        if(Input.GetMouseButtonDown(0) && Time.time > nextFire)
        {
            nextFire = Time.time + reload;
            GameObject temp = Instantiate(weapons[activeWeapon], transform.position + direction.normalized *1f, Quaternion.identity) as GameObject;
            temp.GetComponent<Rigidbody>().velocity = direction.normalized * speed;
        }

    }
}
