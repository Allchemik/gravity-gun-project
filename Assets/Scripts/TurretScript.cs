﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour {

    public GameObject tower;
    public float RotationSmoothing;
    public float targetRotation;
    bool up = true;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Debug.Log("" + tower.transform.rotation.eulerAngles.x);
        if(up)
        {
            if(tower.transform.rotation.eulerAngles.x < targetRotation || tower.transform.rotation.eulerAngles.x >(360-targetRotation))
            {
                tower.transform.RotateAround(tower.GetComponent<Collider>().transform.position, Vector3.back, RotationSmoothing * Time.deltaTime);
            }
            else
            {
                up = false;
                tower.transform.RotateAround(tower.GetComponent<Collider>().transform.position, Vector3.back, -RotationSmoothing * Time.deltaTime);
            }
        }
        else
        {
            if (tower.transform.rotation.eulerAngles.x < targetRotation || tower.transform.rotation.eulerAngles.x > (360 - targetRotation))
            {
                tower.transform.RotateAround(tower.GetComponent<Collider>().transform.position, Vector3.back, -RotationSmoothing * Time.deltaTime);
            }
            else
            {
                up = true;
                tower.transform.RotateAround(tower.GetComponent<Collider>().transform.position, Vector3.back, RotationSmoothing * Time.deltaTime);
            }
        }
	}
}
