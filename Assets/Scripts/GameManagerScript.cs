﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {

    public float TimeLeft;
    public Text TimeLeftText;
    public Text GameOverText;
    public Text WeaponText;
    public Image FadeImage;


    Animator anim;
    float restartTimer;
    float restartDelay = 5f;

    bool countdown = false;
    

	// Use this for initialization
	void Start () {
        
        anim = FadeImage.GetComponent<Animator>();
        hideInfos();
    }
	
	// Update is called once per frame
	void Update () {

        if (countdown)
        {
            TimeLeft -= Time.deltaTime;
        }
        TimeLeftText.text = "Time Left: " + Mathf.RoundToInt(TimeLeft) + "s";

        if(TimeLeft<=0)
        {
            Lost();
            //SceneManager.LoadScene("Gameplay", LoadSceneMode.Single);
        }
	}

    public IEnumerator Victory()
    {
        countdown = false;
        FadeImage.enabled=true;
        GameOverText.text = "You Survived!";

        anim.SetTrigger("GameOver");

        yield return new WaitForSeconds(restartDelay);

        ProgramManagerScript.instance.loadNextLevel();
    }

   public void Lost()
    {
        countdown = false;
        FadeImage.enabled = true;
        GameOverText.text = "You Died!";
        anim.SetTrigger("GameOver");
        restartTimer += Time.deltaTime;
        if(restartTimer>=restartDelay)
        {
            ProgramManagerScript.instance.ReloadLevel();
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }
    }

    public void StartGame()
    {
        countdown = true;
        FadeImage.enabled = false;
        showInfos();
    }

    public void hideInfos()
    {
        TimeLeftText.enabled = false;
        WeaponText.enabled = false;
    }

    public void showInfos()
    {
        TimeLeftText.enabled = true;
        WeaponText.enabled = true;
    }

}
