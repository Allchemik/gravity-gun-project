﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingManagerScript : MonoBehaviour {


    public Image background;
    public Text LevelTitle;
    public Text Levelinfo;
    public Text continueText;

    AsyncOperation loading;

    private void Awake()
    {
        LevelTitle.text = "level ";
        Levelinfo.text = " shot gravity bombs";
        continueText.text = "press space toContinue";
        continueText.enabled = false;
    }

    public void SetTitle(string title)
    {
        LevelTitle.text = title;
    }

    public void SetInfo(string info)
    {
        Levelinfo.text = info;
    }

    public void SetContitnue()
    {
        continueText.text = "press space toContinue";
        continueText.enabled = true;
    }

    private void Update()
    {
        
    }


}
