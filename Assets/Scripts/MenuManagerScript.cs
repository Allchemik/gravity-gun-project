﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManagerScript : MonoBehaviour {

    public void StartNewGame()
    {
        ProgramManagerScript.instance.StartGame();
    }
    public void Quit()
    {
        Application.Quit();
    }
}
