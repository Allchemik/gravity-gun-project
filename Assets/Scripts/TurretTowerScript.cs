﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretTowerScript : MonoBehaviour {

    public GameObject Bolt;
    public Transform nozzleEnd;
    public float refireTime;
    float nextFire;


	// Use this for initialization
	void Start () {

        nextFire = Time.time + refireTime;

	}
	
	// Update is called once per frame
	void Update () {
        if(Time.time>nextFire)
        {
            GameObject temp = Instantiate(Bolt, nozzleEnd.position, Quaternion.Euler(0f, 0f, transform.rotation.eulerAngles.x)) as GameObject;
            
            temp.GetComponent<Rigidbody>().velocity = transform.rotation * Vector3.forward * temp.GetComponent<BoltScript>().speed ;
            nextFire = Time.time + refireTime;
        }
	}
}
