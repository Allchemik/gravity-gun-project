﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltScript : MonoBehaviour {

    public float speed;
   // Rigidbody rb;

	void Start () {
       // rb = GetComponent<Rigidbody>();
        //rb.velocity = new Vector3(-speed,0f,0f);
	}

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
        if(collision.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<GameManagerScript>().Lost();
            DestroyImmediate(collision.gameObject);
        }
    
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
        if (other.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<GameManagerScript>().Lost();
            DestroyImmediate(other.gameObject);
        }
    }


}
