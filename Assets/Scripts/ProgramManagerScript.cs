﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ProgramManagerScript : MonoBehaviour
{
    public static ProgramManagerScript instance = null;

    public Image background;
    public Text LevelTitle;
    public Text Levelinfo;
    public Text continueText;


    public int level;

    public string currentStageName;
    public string[] plansze;


    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        plansze = new string[10];
        for(int i=1;i<11;++i)
        {
            plansze[i-1] = "scene" + i;
        }

        continueText.text = "nacisnij spacje aby rozpoczac";
    }

    private void Start()
    {
        hideSplash();
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("menu");
    }

    // Update is called once per frame
    void Update()
    {
        if(continueText.enabled)
        {
            if(Input.GetButtonDown("Jump"))
            {
                GameObject temp = GameObject.FindGameObjectWithTag("stageManager");
                temp.GetComponent<GameManagerScript>().StartGame();
                hideSplash();
            }
        }
    }

    public void StartGame()
    {
        level = -1;
        loadNextLevel();
    }

    void showsplash()
    {
        background.enabled = true;
        LevelTitle.enabled = true;
        Levelinfo.enabled = true;
    }

    void hideSplash()
    {
        background.enabled = false;
        LevelTitle.enabled = false;
        Levelinfo.enabled = false;
        continueText.enabled = false;
    }

    void setSplashInfo(string title,string info)
    {
        LevelTitle.text = title;
        Levelinfo.text = info;
    }

    void setContitnue()
    {
        continueText.enabled = true;
    }

    public void loadNextLevel()
    {
        ++level;
        if (level > (plansze.Length-1))
        {
            setSplashInfo("wygrales!", "Niestety to juz koniec./n /n dziekuje ze chciales zagrac");
            showsplash();
            StartCoroutine(loadlastScene());
        }
        else
        {
            setSplashInfo("plansza: " + (level + 1), "ppm - strzal bombą\n lpm - wybuch bomby\n spacja - zmiana broni\n dostan sie do wyjscia(bialy kwadrat)\n zanim skonczy sie czas");
            showsplash();
            StartCoroutine(LoadYourAsyncScene());
        }
    }

    public void ReloadLevel()
    {
            //setSplashInfo("plansza: " + (level + 1), "strzlaj bombami grawitacyjnymi");
            showsplash();
            StartCoroutine(LoadYourAsyncScene());
    }

    public void LoadMenu()
    {
        SceneManager.LoadSceneAsync("menu");
    }

    IEnumerator LoadYourAsyncScene()
    {

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(plansze[level]);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        setContitnue();
    }

    IEnumerator loadlastScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("End");
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        setContitnue();
    }

}