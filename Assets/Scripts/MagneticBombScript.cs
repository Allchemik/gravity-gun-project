﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticBombScript : MonoBehaviour {

    public float lifetime = 1.5f;
    public float blastRange = 5;
    bool exploded = false;
    float DestroyTime;


    GameObject[] _Objects;
    GameObject _Player;

    MeshRenderer _obrazek;

    // Use this for initialization
    void Start()
    {
        //_Objects = FindObjectsOfType<Magnetic>();

        Magnetic[] temp = FindObjectsOfType<Magnetic>();
        _Objects = new GameObject[temp.Length];

        for (int i = 0; i < temp.Length; ++i)
        {
            _Objects[i] = temp[i].getGameObj();
        }

        _Player = GameObject.FindGameObjectWithTag("Player");
        _obrazek = GetComponent<MeshRenderer>();
    }


    // Update is called once per frame
    void Update()
    {

        if (!exploded)
        {
            if (Input.GetMouseButtonDown(1))
            {
                Boom();
            }
        }
        else
        {
            if (Time.time < DestroyTime)
            {
                float remainTime = (DestroyTime - Time.time) / lifetime;
                for (int i = 0; i < _Objects.Length; ++i)
                {
                    Vector3 direction = transform.position - _Objects[i].transform.position;
                    //Mathf.Max(0f, (Time.time - DestroyTime) / lifetime)
                    float magnify = Mathf.Max(1f, blastRange / (Mathf.Abs(direction.magnitude)));
                    _Objects[i].GetComponent<Rigidbody>().AddForce(direction.normalized * magnify * Mathf.Max(0f, remainTime));
                }
                /*if (_Player != null)
                {
                    Vector3 direction = transform.position - _Player.transform.position;
                    float magnify = Mathf.Max(1f, blastRange / (Mathf.Abs(direction.magnitude)));
                    _Player.GetComponent<Rigidbody>().AddForce(direction.normalized * magnify * Mathf.Max(0f, remainTime));
                }*/
                float relation = blastRange * remainTime;
                transform.localScale = new Vector3(relation, relation, 0.25f);
                _obrazek.material.color = new Color(remainTime, 0f, 0f, remainTime);
            }
            else
            {
                DestroyImmediate(gameObject);
            }
        }
    }

    public void Boom()
    {
        if (!exploded)
        {
            exploded = true;
            DestroyTime = Time.time + lifetime;
            GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
            GetComponent<Collider>().enabled = false;
            _obrazek.material.color = Color.red;
        }
    }
}
